package com.example.recyclerview;

import android.widget.ImageView;
import android.widget.TextView;

public class Product_class {


    private int image ;
    private String Product_Name;
    private String  Price;
    private String  Store;

    public Product_class(int image, String product_Name, String price, String store) {
        this.image = image;
        Product_Name = product_Name;
        Price = price;
        Store = store;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getProduct_Name() {
        return Product_Name;
    }

    public void setProduct_Name(String product_Name) {
        Product_Name = product_Name;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }



    public String getStore() {
        return Store;
    }

    public void setStore(String store) {
        Store = store;
    }
}
