package com.example.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerviewTowAdapter extends RecyclerView.Adapter<RecyclerviewTowAdapter.MyProductHolder> {

    ArrayList<Product_class>product;

    public RecyclerviewTowAdapter(ArrayList<Product_class> product_classes) {
        this.product = product_classes;
    }

    @NonNull
    @Override
    public MyProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View vv= LayoutInflater.from(parent.getContext()).inflate(R.layout.product_activity,null,false);
       MyProductHolder productHolder =new MyProductHolder(vv);
        return productHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyProductHolder holder, int position) {
        Product_class p =product.get(position);
        holder.image.setImageResource(p.getImage());
        holder.Product_Name.setText(p.getProduct_Name());
        holder.Price.setText(p.getPrice());
        holder.Store.setText(p.getStore());

        if(position %2==0){
            holder.linearLayout.setBackgroundResource(R.color.colorAccent);
        } else{
            holder.linearLayout.setBackgroundResource(R.color.colorP);

        }

    }

    @Override
    public int getItemCount() {
        return product.size();
    }

    class MyProductHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView Product_Name;
        TextView  Price;
        TextView  Store;
        LinearLayout linearLayout;

        public MyProductHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.product_Image);
            Product_Name = itemView.findViewById(R.id.product_tv_pro);
            Store = itemView.findViewById(R.id.product_tv_sto);
            Price = itemView.findViewById(R.id.product_tv_Pri);
            linearLayout = itemView.findViewById(R.id.linear);


        }
    }
}
