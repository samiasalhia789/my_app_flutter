package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    RecyclerView vie1, viw2;
    CheckBox cb_remember ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = findViewById(R.id.main_title);

        vie1 = findViewById(R.id.main_rec_one);
        viw2 = findViewById(R.id.main_rec_tow);
        cb_remember = findViewById(R.id.cb_remember);

        //RecyclerviewAdapter
        ArrayList<Category_class> list = new ArrayList<>();
        list.add(new Category_class("Makeup"));
        list.add(new Category_class("shoes"));
        list.add(new Category_class("Women's Bag"));
        list.add(new Category_class("Mobiles"));
        list.add(new Category_class("Hair dyes"));
        list.add(new Category_class("Lenses"));

        RecyclerviewAdapter adapter = new RecyclerviewAdapter(list);
        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,
                false);
        vie1.setLayoutManager(manager);
        vie1.setHasFixedSize(true);
        vie1.setAdapter(adapter);





        //RecyclerviewTowAdapter
        ArrayList<Product_class> list1 = new ArrayList<>();
        list1.add(new Product_class(R.drawable.makup, "Fawndation", "20$", "(Brand)"));
        list1.add(new Product_class(R.drawable.shoes, "Shoes", "25$", "(Alibaba.com)"));
        list1.add(new Product_class(R.drawable.womensbag, "Bag", "30$", "(Stylish)"));
        list1.add(new Product_class(R.drawable.mobiles, "Apple phone", "10$", "(Alibaba.com)"));
        list1.add(new Product_class(R.drawable.hairdyes, "Hair dyes", "10$", "(Brand)"));
        list1.add(new Product_class(R.drawable.lenses, "Lenses", "10$", "(Brand)"));


        RecyclerviewTowAdapter adapter1 = new RecyclerviewTowAdapter(list1);
        LinearLayoutManager manager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);

        viw2.setLayoutManager(manager2);
        viw2.setHasFixedSize(true);
        viw2.setAdapter(adapter1);



         //checkbox
        SharedPreferences shared = getSharedPreferences(CB_SHARED_NAME, MODE_PRIVATE);
        boolean check = shared.getBoolean(CB_key, false);
        if (check==true) {
            // intent to main
        }
        // check remember me
        cb_remember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    SharedPreferences shared = getSharedPreferences(CB_SHARED_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor = shared.edit();
                    editor.putBoolean(CB_key, true);
                    editor.apply();
                } else {
                    SharedPreferences shared = getSharedPreferences(CB_SHARED_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor = shared.edit();
                    editor.putBoolean(CB_key, false);
                    editor.apply();
                }
            }
        });




    }


}
