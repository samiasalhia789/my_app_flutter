package com.example.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerviewAdapter extends RecyclerView.Adapter<RecyclerviewAdapter.MyCategoryHolder> {

    ArrayList<Category_class> classes;

    public RecyclerviewAdapter(ArrayList<Category_class> classes) {
        this.classes = classes;
    }

    @NonNull
    @Override
    public MyCategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View v =  LayoutInflater.from(parent.getContext()).inflate(R.layout.category_activity,null,false);
        MyCategoryHolder holder = new MyCategoryHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyCategoryHolder holder, int position) {
          Category_class c = classes.get(position);
          holder.Name_View.setText(c.getName());
    }

    @Override
    public int getItemCount() {
        return classes.size();
    }

    class MyCategoryHolder extends RecyclerView.ViewHolder {
       TextView Name_View ;
        public MyCategoryHolder(@NonNull View itemView) {
            super(itemView);

            Name_View = itemView.findViewById(R.id.cat_tv_Mak);

        }
    }

}
